Rails.application.routes.draw do
  get 'auth/login'
  get 'auth/signup'
  resources :movies
  resources :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
