class AuthController < ApplicationController
  def login
    @user = User.find_by!(email: param_user[:email])
    if @user && @user.authenticaticate(login_params[:password])
      token = JsonWebToken::Base.encode(user_id: @user.id)

      render json: { token: token }, status: :ok
    else
      render json: @user.errors, status: 401
    end
  end

  def signup
    @user = User.new(param_user)
    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: 422
    end
  end

  private
  def param_user
    return params.require(:user).permite(:name, :last_name, :email, :password, :password_confirmation)
  end

  def login_params
    return params.require(:user).permit(:email, :password)
  end
end
